﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicsProgramminApplication
{
    public class CommandParser
    {

        Boolean error = false;
        string error_cmm;

        
        public CommandParser() { }
        public String Parser(string command, string code, int line)
        {
            
            string comm = command.ToLower();
            string cods = code.ToLower();
            
            switch (comm)
            {
                case "run":
                    try
                    {
                            int temp;
                            String code_line = cods;
                            char[] code_delimiters = new char[] { ' ' };
                            String[] words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                            if (words[0] == "draw")
                            {
                                if (words[1] == "circle")
                                {
                                    if (!(words.Length == 3))
                                    {
                                        error = true;
                                        error_cmm = "The error in on line " + line + " please type correct code for 'draw circle'";
                                        return error_cmm;
                                    }
                                    else if(Int32.TryParse(words[2],out temp))
                                    {
                                        string shape = "circle " + words[2];
                                        return shape;
                                    }
                                    else
                                    {
                                        return "Please enter The parameters in integers in line " + line;

                                    }
                                }
                                else if (words[1] == "square")
                                {
                                    if (!(words.Length == 3))
                                    {
                                        error = true;
                                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw square'";
                                        return error_cmm;
                                    }
                                    else if (Int32.TryParse(words[2], out temp))
                                    {
                                        string shape = "square " + words[2];
                                        return shape;
                                    }
                                    else
                                    {
                                        return "Please enter The parameters in integers in line " + line;  

                                    } 
                                }

                                else if (words[1] == "rectangle")
                                {
                                     if (!(words.Length == 4))
                                     {
                                         error = true;
                                         error_cmm = "The error in on line " + line + " Please type correct code for 'draw rectangle'";
                                          
                                     }
                                    else if (Int32.TryParse(words[2], out temp)&& Int32.TryParse(words[3], out temp))
                                    {
                                        string shape = "rectangle " + words[2]+" "+words[3];
                                        return shape;
                                    }
                                    else
                                    {
                                        return "Please enter The parameters in integers in line " + line;

                                    }
                                }
                                else if (words[1] == "triangle")
                                {
                                    if (!(words.Length == 8))
                                    {
                                        error = true;
                                        error_cmm = "The error in on line " + line + " Please type correct code for 'draw triangle'";

                                    }
                                    else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp) && Int32.TryParse(words[4], out temp) && Int32.TryParse(words[5], out temp) && Int32.TryParse(words[6], out temp) && Int32.TryParse(words[7], out temp))
                                    {
                                        string shape = "triangle " + words[2] + " " + words[3] + " " + words[4] + " " + words[5] + " " + words[6] + " " + words[7];
                                        return shape;
                                    }
                                    else
                                    {
                                        return "Please enter The parameters in integers in line " + line;

                                    }
                                }
                                else
                                {
                                    return "Please enter correct code in line " + line;
                                }
                            }
                            else if (words[0]=="move")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                                    return error_cmm;
                                }
                                else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                                {
                                    return code_line;
                                }
                                else
                                {
                                    return "Please enter The parameters in integers in line "+ line;

                                }
                            }
                            else if (words[0] == "color")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                                    return error_cmm;
                                }
                                else if (Int32.TryParse(words[2], out temp))
                                {
                                    
                                    return code_line;
                                }
                                else
                                {
                                    return "Please enter The parameters in integers";

                                }
                            }
                            else if (words[0] == "fill")
                            {
                                if (!(words.Length == 2))
                                {
                                    error = true;
                                    error_cmm = "The error in on line " + line + " Please type correct code for 'move'";
                                    return error_cmm;
                                }

                            else
                            {
                                return code_line;

                            }
                        }
                        else
                            {
                                return "Please enter The correct codes";
                            }
                        
                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return ex.Message;
                        
                    }
                    catch (FormatException ex)
                    {
                       return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                case "clear":
                    return "clear";
                case "reset":
                    return "reset";
                case "help":
                    return "help";
                default:
                    return "Please enter a command";
                    
            }
            return null;
        }
        
    } 
}
