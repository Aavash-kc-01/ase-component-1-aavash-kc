﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    interface Shapes
    {
        void setColor(Color c);
        void setX(int x);
        void setY(int y);
        int getX();
        int getY();
        void draw(Graphics g,Color c, int thickness);
       
    }
}
