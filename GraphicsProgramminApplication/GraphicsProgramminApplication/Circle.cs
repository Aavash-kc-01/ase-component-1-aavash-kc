﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Circle shape defination class
    /// </summary>
    public class Circle : Shape
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        int radius;

        /// <summary>
        /// Constructor method
        /// </summary>
        public Circle() : base()
        {

        }

        /// <summary>
        /// parameterised constructor
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        public Circle(Color colour, int x, int y, int radius) : base(colour, x, y)
        {

            this.radius = radius;
        }

        /// <summary>
        /// method to set radius of the circle
        /// </summary>
        /// <param name="radius"></param>
        public void setRadius(int radius)
        {
            this.radius = radius;


        }

        /// <summary>
        /// Method to return the value of radius
        /// </summary>
        /// <returns></returns>
        public int getRadius()
        {
            return radius;
        }

        /// <summary>
        /// Draw method for a circle
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g,Color c, int thickness)
        {

            Pen p = new Pen(c, thickness);
            SolidBrush b = new SolidBrush(colour);
            g.FillEllipse(b, x, y, radius * 2, radius * 2);
            g.DrawEllipse(p, x, y, radius * 2, radius * 2);

        }

     

        public override string ToString() 
        {
            return base.ToString() + "  " + this.radius;
        }
    }
}
