﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Class defination for rectangle shape 
    /// </summary>
    public class Rectangle:Shape
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        int width, height;

        /// <summary>
        /// constructor
        /// </summary>
        public Rectangle() : base()
        {
            width = 100;
            height = 100;
        }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Rectangle(Color colour, int x, int y, int width, int height) : base(colour, x, y)
        {

            this.width = width;
            this.height = height;
        }

        /// <summary>
        /// method to set height of rectangle
        /// </summary>
        /// <param name="height"></param>
        public void setHeight(int height)
        {
            this.height = height;
            

        }

        /// <summary>
        /// method to set width of rectangle
        /// </summary>
        /// <param name="width"></param>
        public void setWidth(int width)
        {
            this.width = width;


        }

        /// <summary>
        /// method to get height of rectangle
        /// </summary>
        /// <returns></returns>
        public int getHeight()
        {
            return height;


        }

        /// <summary>
        /// method to get width of rectangle
        /// </summary>
        /// <returns></returns>
        public int getWidth()
        {
            return height;


        }


        /// <summary>
        /// draw method for rectangle
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            Pen p = new Pen(c, thickness);
            SolidBrush b = new SolidBrush(colour);
            g.FillRectangle(b, x, y, width, height);
            g.DrawRectangle(p, x, y, width, height);
        }

        
    }
}
