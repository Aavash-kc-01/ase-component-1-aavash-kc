﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// Triangle shape defination class
    /// </summary>
    class Triangle :Shape
    {
        /// <summary>
        /// Variable declarations
        /// </summary>
        PointF p1, p2, p3;

        /// <summary>
        /// Method to set the int params to pointf variables
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="point3"></param>
        /// <param name="point4"></param>
        /// <param name="point5"></param>
        /// <param name="point6"></param>
        public void setPoints (int point1,int point2,int point3,int point4, int point5, int point6)
        {
            this.p1 = new PointF(point1, point2);
            this.p2 = new PointF(point3, point4);
            this.p3 = new PointF(point5, point6);
        }

        /// <summary>
        /// draw method for triangle
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        public override void draw(Graphics g, Color c, int thickness)
        {
            
            Pen p = new Pen(c, thickness);
            PointF[] curvPoints =
            {
                p1,p2,p3
            };
            g.DrawPolygon(p, curvPoints);
            
        }

       
    }
}
