﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GraphicsProgramminApplication
{
    
    public partial class Form1 : Form
    {
        Boolean saveChange;
        Color c ;
        Color fill;
        int moveX, moveY;
        int thickness;
        Boolean drawCircle;
        Boolean drawRectangle;
        Boolean drawSquare;
        Boolean drawTriangle;
        String filePath;
        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Square> squareObjects;
        List<Triangle> triangleObjects;
        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            circleObjects = new List<Circle>(); 
            rectangleObjects = new List<Rectangle>();
            squareObjects = new List<Square>();
            triangleObjects = new List<Triangle>();
            c = Color.Black;
            fill = Color.Transparent;
            thickness = 2;
        }

        private void btnExecute_Click(object sender, EventArgs e)
        {
            string cmm = txtCommand.Text;
            string cod = txt_code.Text;
            
            char[] delimiters = new char[] { '\r', '\n' };
            string[] parts = cod.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < parts.Length; i++)
            {
                CommandParser cm = new CommandParser();
                string parsedCode = cm.Parser(cmm, parts[i],i+1);
                char[] code_delimiters = new char[] { ' ' };
                string[] words = parsedCode.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                switch (words[0])
                {
                    case "circle":
                        Circle circle = new Circle();
                        circle.setColor(fill);
                        circle.setX(moveX);
                        circle.setY(moveY);
                        circle.setRadius(Convert.ToInt32(words[1]));
                        circleObjects.Add(circle);
                        drawCircle = true;
                        break;
                    case "rectangle":
                        Rectangle rectangle = new Rectangle();
                        rectangle.setColor(fill);
                        rectangle.setX(moveX);
                        rectangle.setY(moveY);
                        rectangle.setHeight(Convert.ToInt32(words[1]));
                        rectangle.setWidth(Convert.ToInt32(words[2]));
                        rectangleObjects.Add(rectangle);
                        drawRectangle = true;
                        break;
                    case "square":
                        Square square = new Square();
                        square.setColor(fill);
                        square.setX(moveX);
                        square.setY(moveY);
                        square.setSize(Convert.ToInt32(words[1]));
                        squareObjects.Add(square);
                        drawSquare = true;
                        break;
                    case "triangle":
                        Triangle triangle = new Triangle();
                        triangle.setColor(fill);
                        triangle.setPoints(Convert.ToInt32(words[1]), Convert.ToInt32(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]));
                        triangleObjects.Add(triangle);
                        drawTriangle = true;
                        break;
                    case "clear":
                        circleObjects.Clear();
                        rectangleObjects.Clear();
                        squareObjects.Clear();
                        triangleObjects.Clear();
                        this.drawCircle = false;
                        this.drawRectangle = false;
                        this.drawSquare = false;
                        this.drawTriangle = false;
                        txt_code.Clear();
                        pnl_canvas.Refresh();
                        break;
                    case "move":
                        this.moveX = Convert.ToInt32(words[1]);
                        this.moveY = Convert.ToInt32(words[2]);
                        break;
                    case "reset":
                        circleObjects.Clear();
                        rectangleObjects.Clear();
                        squareObjects.Clear();
                        triangleObjects.Clear();
                        this.moveX = 0;
                        this.moveY = 0;
                        this.thickness = 2;
                        this.fill = Color.Transparent;
                        c = Color.Black;
                        pnl_canvas.Refresh();
                        break;
                    case "color":
                        this.thickness = Convert.ToInt32(words[2]);
                        Color cs = Color.FromName(words[1]);
                        
                        if (cs.IsKnownColor){
                            this.c = Color.FromName(char.ToUpper(words[1][0])+ words[1].Substring(1));
                        }
                        else
                        {
                            MessageBox.Show("please enter a valid color");
                        }
                        break;
                    case "fill":
                        Color fil = Color.FromName(words[1]);
                        if (fil.IsKnownColor)
                        {
                            this.fill = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                        }
                        else if (words[1] == "no")
                        {
                            this.fill = Color.Transparent;
                            pnl_canvas.Refresh();
                        }
                        else
                        {
                            MessageBox.Show("please enter a valid color");
                        }
                        break;
                    case "help":
                        MessageBox.Show("For Your Help:\n" +
                         "draw circle 100\n" +
                         "draw rectangle 100 50\n" +
                         "draw triangle 10 10 100 10 50 60\n" +
                         "draw square 50\n" +
                         "move 100 100\n" +
                         "color red 23\n" +
                         "fill red\n" +
                         "fill no\n");
                        break;
                    default:
                        MessageBox.Show(parsedCode);
                        break;

                }
            }
            pnl_canvas.Refresh();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(saveChange == false)
            {
                //when there is no unsaved existing project
                try
                {
                    SaveFileDialog sf = new SaveFileDialog();
                    sf.Filter = "Text Document(*.txt)|*.txt|All Files(*.*)|*.*";
                    if (sf.ShowDialog() == DialogResult.OK)
                    {
                        System.IO.File.WriteAllText(sf.FileName, txt_code.Text);
                    }

                    filePath = Path.GetFullPath(sf.FileName);


                }
                catch (Exception)
                {
                    MessageBox.Show("File couldn't be saved");
                }
            }
            else
            {

                DialogResult dialogResult = MessageBox.Show("Do you want to save changes to existing project before creating new project.", "Unsaved Changes", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {

                    try
                    {
                        System.IO.File.WriteAllText(filePath, txt_code.Text);
                        saveChange = false;
                        MessageBox.Show("Saved Sucessfully");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error while saving file.");
                    }
                }
                try
                {
                    SaveFileDialog sf = new SaveFileDialog();
                    sf.Filter = "Text Document(*.txt)|*.txt|All Files(*.*)|*.*";
                    if (sf.ShowDialog() == DialogResult.OK)
                    {
                        System.IO.File.WriteAllText(sf.FileName, txt_code.Text);
                    }

                    filePath = Path.GetFullPath(sf.FileName);

                    saveChange = false;
                }
                catch (Exception)
                {

                    MessageBox.Show("Error while saving file.");
                }
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("For Your Help:\n" +
                "draw circle 100\n" +
                "draw rectangle 100 50\n" +
                "draw triangle 10 10 100 10 50 60\n" +
                "draw square 50\n" +
                "move 100 100\n" +
                "color red 23\n" +
                "fill red\n" +
                "fill no\n");
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            if (op.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txt_code.Text = File.ReadAllText(op.FileName);
                filePath = Path.GetFullPath(op.FileName);
                saveChange = true;
            }
        }

        private void pnl_canvas_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (drawCircle == true)
            {
                foreach (Circle circleObject in circleObjects)
                {
                    circleObject.draw(g, c, thickness);
                }
            }
            if (drawRectangle == true)
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    rectangleObject.draw(g, c, thickness);
                }
            }
            if (drawSquare == true)
            {
                foreach (Square squareObject in squareObjects)
                {
                    squareObject.draw(g, c, thickness);
                }
            }
            if (drawTriangle == true)
            {
                foreach(Triangle triangleObject in triangleObjects)
                {
                    triangleObject.draw(g, c, thickness);
                }
            }
        }
    }
}
