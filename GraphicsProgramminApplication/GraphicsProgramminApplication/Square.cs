﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace GraphicsProgramminApplication
{
    /// <summary>
    /// class defination got square shape
    /// </summary>
    public class Square:Rectangle
    {
        /// <summary>
        /// variable decleration
        /// </summary>
        private int size;

        /// <summary>
        /// constructor
        /// </summary>
        public Square() : base()
        {

        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="colour"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="size"></param>
        public Square(Color colour, int x, int y, int size) : base(colour, x, y, size, size)
        {
            this.size = size;
        }

        /// <summary>
        /// method to set the size of square
        /// </summary>
        /// <param name="size"></param>
        public void setSize(int size)
        {
            this.size = size;
        }

        /// <summary>
        /// method to get square size
        /// </summary>
        /// <returns></returns>
        public int getSize()
        {
            return size;
        }

    }
}
